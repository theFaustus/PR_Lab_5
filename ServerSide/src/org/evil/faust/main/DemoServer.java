package org.evil.faust.main;

import org.evil.faust.utils.MathServer;

/**
 * Created by Faust on 4/8/2017.
 */
public class DemoServer {
    public static void main(String[] args) {
        MathServer mathServer = new MathServer(314);
        mathServer.startListening();
    }
}
