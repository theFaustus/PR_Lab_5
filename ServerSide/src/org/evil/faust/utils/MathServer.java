package org.evil.faust.utils;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Faust on 4/8/2017.
 */
public class MathServer {
    private int port;
    private ServerSocket serverSocket;
    private ExecutorService executor = Executors.newFixedThreadPool(10);
    static Map<String, Socket> connectedUsers = new HashMap<>();

    public MathServer(int port) {
        System.out.println("Goodies Server 2.0");
        System.out.println("Listening on port " + this.port);
        try {
            this.port = port;
            this.serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            System.out.println("System exception!");
        }
    }

    public void startListening() {
        try {
            while (true) {
                Socket s = serverSocket.accept();
                System.out.println("Connection established!");
                this.executor.execute(new ClientHandler(s));
            }
        } catch (Exception e) {
            System.out.println("System exception!");
        }
    }

}
