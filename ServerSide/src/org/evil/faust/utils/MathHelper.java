package org.evil.faust.utils;

import javafx.util.Pair;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Faust on 4/9/2017.
 */
public class MathHelper {
    private static final Map<String, Double> CONSTANTS = Stream.of(
            new Pair<>("One", 1.0),
            new Pair<>("Zero", 0.0),
            new Pair<>("\u03C0", 3.1415926535897932384626433),
            new Pair<>("e", 2.7182818284590452353602874),
            new Pair<>("\u1D67", 0.5772156649015328606065120),
            new Pair<>("\u03D5", 1.6180339887498948482045868),
            new Pair<>("\u2133", 0.2614972128476427837554268),
            new Pair<>("\u03B2", 0.28016949902386913303),
            new Pair<>("\u03C3", 0.3532363718549959845435165),
            new Pair<>("\u2113", 0.5),
            new Pair<>("\u03A9", 0.5671432904097838729999686),
            new Pair<>("\u03BB", 0.3036630028987326585974481),
            new Pair<>("\u03C1", 1.3247179572447460259609088),
            new Pair<>("\u03BC", 1.4513692348833810502839684),
            new Pair<>("\u03B1", 2.5029078750958928222839028),
            new Pair<>("\u03A8", 3.3598856662431775531720113))
            .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

    public static String getConstants() {
        String result = "";
        for (Map.Entry e : CONSTANTS.entrySet()) {
            result += result.isEmpty() ? (String.format("%s = %f", e.getKey(), e.getValue())) : (String.format("\n%s = %f", e.getKey(), e.getValue()));
        }
        return result;
    }

    public static double evaluateBinaryExpression(String expression) {
        double result = 0.0;
        double firstOperand;
        double secondOperand;
        String[] tokens = expression.split(" ");
        try {
            firstOperand = Double.parseDouble(tokens[1]);
            secondOperand = Double.parseDouble(tokens[3]);
        } catch (Exception e) {
            return Double.NaN;
        }

        switch (tokens[2]) {
            case "+":
                result = firstOperand + secondOperand;
                break;
            case "-":
                result = firstOperand - secondOperand;
                break;
            case "*":
                result = firstOperand * secondOperand;
                break;
            case "/":
                result = firstOperand / secondOperand;
                break;
            case "^":
                result = Math.pow(firstOperand, secondOperand);
                break;

        }

        return result;
    }

    public static double evaluateUnaryExpression(String expression) {
        double result = 0.0;
        double operand;
        String[] tokens = expression.split(" ");
        try {
            tokens[2] = tokens[2].replace("(", "").replace(")", "");
            operand = Double.parseDouble(tokens[2]);
        } catch (Exception e) {
            return Double.NaN;
        }

        switch (tokens[1]) {
            case "sin":
                result = Math.sin(operand);
                break;
            case "cos":
                result = Math.cos(operand);
                break;
            case "tan":
                result = Math.tan(operand);
                break;
            case "atan":
                result = Math.atan(operand);
                break;
            case "rcpr":
                result = 1 / operand;
                break;
            case "sqr":
                result = Math.pow(operand, 2);
                break;
            case "sqrt":
                result = Math.sqrt(operand);
                break;
            case "abs":
                result = Math.abs(operand);
                break;
            case "log":
                result = Math.log(operand);
                break;
            case "log10":
                result = Math.log10(operand);
                break;
            case "acos":
                result = Math.acos(operand);
                break;
            case "asin":
                result = Math.asin(operand);
                break;
        }
        return result;
    }

    public static boolean isEvaluableBinary(String expression) {
        double firstOperand;
        double secondOperand;
        String[] tokens = expression.split(" ");
        try {
            firstOperand = Double.parseDouble(tokens[1]);
            secondOperand = Double.parseDouble(tokens[3]);
        } catch (Exception e) {
            return false;
        }

        if (tokens.length != 4) {
            return false;
        }

        if (!tokens[2].equals("+") && !tokens[2].equals("-") && !tokens[2].equals("/") && !tokens[2].equals("*")
                && !tokens[2].equals("^"))
            return false;

        return true;
    }

    public static boolean isEvaluableUnary(String expression) {
        double operand;
        String[] tokens = expression.split(" ");
        try {
            tokens[2] = tokens[2].replace("(", "").replace(")", "");
            operand = Double.parseDouble(tokens[2]);
        } catch (Exception e) {
            return false;
        }

        if (tokens.length != 3) {
            return false;
        }

        if (!tokens[1].equals("sin") && !tokens[1].equals("cos") && !tokens[1].equals("tan") && !tokens[1].equals("atan")
                && !tokens[1].equals("sqr") && !tokens[1].equals("sqrt") && !tokens[1].equals("rcpr") && !tokens[1].equals("log") && !tokens[1].equals("log10")
                && !tokens[1].equals("abs") && !tokens[1].equals("asin") && !tokens[1].equals("acos"))
            return false;
        return true;
    }

    public static boolean isEvaluableSpecial(String expression) {
        int operand;
        String[] tokens = expression.split(" ");
        try {
            operand = Integer.parseInt(tokens[1]);
        } catch (Exception e) {
            return false;
        }
        if (tokens.length != 2) {
            return false;
        }

        if (!tokens[0].equals("fibonacci") && !tokens[0].equals("palindrome") && !tokens[0].equals("prime"))
            return false;
        return true;
    }

    public static ArrayList<Integer> getFibonacciSequence(String expression) {
        String[] tokens = expression.split(" ");
        int limit = Integer.parseInt(tokens[1]);
        ArrayList<Integer> sequence = new ArrayList<>();
        for (int i = 1; i <= limit; i++) {
            sequence.add(fibonacci(i));
        }
        return sequence;
    }

    public static ArrayList<Integer> getPrimeSequence(String expression) {
        String[] tokens = expression.split(" ");
        int limit = Integer.parseInt(tokens[1]);
        ArrayList<Integer> sequence = new ArrayList<>();
        for (int i = 1; i <= limit; i++) {
            if (isPrime(i))
                sequence.add(i);
        }
        return sequence;
    }

    public static ArrayList<Integer> getPalindromeSequence(String expression) {
        String[] tokens = expression.split(" ");
        int limit = Integer.parseInt(tokens[1]);
        ArrayList<Integer> sequence = new ArrayList<>();
        for (int i = 1; i <= limit; i++) {
            if (isPalindrome(i))
                sequence.add(i);
        }
        return sequence;
    }


    public static int fibonacci(int number) {
        if (number == 1 || number == 2) {
            return 1;
        }
        return fibonacci(number - 1) + fibonacci(number - 2);
    }

    public static boolean isPalindrome(int number) {
        int palindrome = number;
        int reverse = 0;

        while (palindrome != 0) {
            int remainder = palindrome % 10;
            reverse = reverse * 10 + remainder;
            palindrome = palindrome / 10;
        }

        if (number == reverse) {
            return true;
        }
        return false;
    }

    public static boolean isPrime(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }


}