package org.evil.faust.utils;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.evil.faust.utils.MathServer.connectedUsers;

/**
 * Created by Faust on 4/8/2017.
 */
public class ClientHandler implements Runnable {

    private static final String BYE_COMMAND = "bye";
    private static final String REGISTER_COMMAND = "register";
    private static final String LOG_IN_COMMAND = "log in";
    private static final String HELP_COMMAND = "help";
    private static final String CLR_HISTORY_COMMAND = "clr history";
    private static final String MSG_COMMAND = "msg";
    private static final String CREATE_GROUP_COMMAND = "create group";
    private static final String DELETE_GROUP_COMMAND = "delete group";
    private static final String ADD_USER_IN_GROUP_COMMAND = "add";
    private static final String FIBONACCI_COMMAND = "fibonacci";
    private static final String PRIME_COMMAND = "prime";
    private static final String PALINDROME_COMMAND = "palindrome";
    private static final String CALC_COMMAND = "calc";
    private static final String HISTORY_COMMAND = "history";
    private static final String CONSTANTS_COMMAND = "constants";
    public static final String MSG_GROUP_COMMAND = "msg group";

    private Socket socket;
    private BufferedReader input;
    private PrintWriter output;
    private String history = "";
    private Map<String, String> HELP_COMMANDS = new LinkedHashMap<>(15, 0.75f, true);
    private String loggedInUsername = "";
    private boolean isGroupAdministrativeRightsSet = false;
    private static Map<String, Map<String, Socket>> groups = new HashMap<>();

    public ClientHandler(Socket socket) {
        HELP_COMMANDS.put("HELP", " - list all available commands.");
        HELP_COMMANDS.put("LOG IN", " - authenticate yourself in system. You`ll be requested for registered USERNAME and PASSWORD.");
        HELP_COMMANDS.put("REGISTER", " - register yourself in system. You`ll be requested for USERNAME and PASSWORD.");
        HELP_COMMANDS.put("MSG \"{message}\"", " - send a public message to everybody.");
        HELP_COMMANDS.put("MSG {username} \"{message}\"", " - send a private message to a specified user.");
        HELP_COMMANDS.put("MSG GROUP {group_name} \"{message}\"", " - send a private message to all users from an existing group.");
        HELP_COMMANDS.put("CREATE GROUP {group_name}", " - create a private group for discussing and obtain the administrative rights for this group.");
        HELP_COMMANDS.put("ADD {username} IN GROUP {group_name}", " - add a specified user into the previous created group for discussing.");
        HELP_COMMANDS.put("DELETE GROUP {group_name}", " - delete a previous created private group. You need the administrative rights for this group.");
        HELP_COMMANDS.put("CALC [<{a} {operator} {b}> | <{function} {(a)}>]", " - evaluate a basic math expression with given parameteres. " +
                "\n\tCheck below the available functions and operators:" +
                "\n\t\t -> function:" +
                "\n\t\t\t --> sin = returns the trigonometric sine of an angle." +
                "\n\t\t\t --> asin = returns the arc sine of a value; the returned angle is in the range -pi/2 through pi/2." +
                "\n\t\t\t --> cos = returns the trigonometric cosine of an angle." +
                "\n\t\t\t --> acos = returns the arc cosine of a value; the returned angle is in the range 0.0 through pi." +
                "\n\t\t\t --> tan = returns the trigonometric tangent of an angle." +
                "\n\t\t\t --> atan = returns the trigonometric arc tangent of an angle. The returned angle is in the range -pi/2 through pi/2." +
                "\n\t\t\t --> rcpr = returns the reciprocal of a value." +
                "\n\t\t\t --> sqr = returns the value of the argument raised to the power of 2." +
                "\n\t\t\t --> sqrt = returns the correctly rounded positive square root of a value." +
                "\n\t\t\t --> abs = returns the absolute value of a value." +
                "\n\t\t\t --> log = returns the natural logarithm (base e) of a value." +
                "\n\t\t\t --> log10 = returns the base 10 logarithm of a value." +
                "\n\t\t -> operator:" +
                "\n\t\t\t --> + = plus sign." +
                "\n\t\t\t --> - = minus sign." +
                "\n\t\t\t --> * = multiplication sign." +
                "\n\t\t\t --> / = divide sign." +
                "\n\t\t\t --> ^ = power sign.");
        HELP_COMMANDS.put("PRIME {limit}", " - list of all prime numbers bounded by the chosen limit.");
        HELP_COMMANDS.put("PALINDROME {limit}", " - list of all palindrome numbers bounded by the chosen limit.");
        HELP_COMMANDS.put("FIBONACCI {limit}", " - list of all fibonacci numbers bounded by the chosen limit.");
        HELP_COMMANDS.put("CONSTANTS", " - list of mathematical constants.");
        HELP_COMMANDS.put("HISTORY", " - list all executed math operations.");
        HELP_COMMANDS.put("CLR HISTORY", " - clear the current history of executed math operations.");
        HELP_COMMANDS.put("BYE", " - exit the system.");
        HELP_COMMANDS.put("<#END#>", " - notation for the end of server`s response.\n");
        this.socket = socket;
    }

    @Override
    public void run() {
        String client = socket.getInetAddress().toString();
        System.out.println("Connected to " + client + " " + Thread.currentThread().getName());
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
            output.println("Welcome to Math Server v.1.0");
            output.println("Enter REGISTER or LOG IN to continue. Enter BYE to exit.\n#END#");
            while (true) {
                if (input.ready()) {
                    String command = input.readLine();
                    System.out.println(command);
                    if (command.equalsIgnoreCase(BYE_COMMAND)) {
                        System.out.println(command);
                        output.println("Bye, bye.\n#END#");
                        break;
                    } else if (command.equalsIgnoreCase(REGISTER_COMMAND)) {
                        char[] username = getUsername();
                        loggedInUsername = String.valueOf(username);
                        char[] password = getPassword();
                        AccountHelper accountHelper = new AccountHelper();
                        System.out.println(connectedUsers);
                        if (!isUserOnline(loggedInUsername)) {
                            if (accountHelper.isUsernameAvailable(String.valueOf(username))) {
                                if (accountHelper.registerUser(username, password)) {
                                    output.println(loggedInUsername + " successfully registered, go ahead. Enter HELP to see all available commands.\n#END#");
                                    connectedUsers.put(loggedInUsername, socket);
                                    break;
                                } else {
                                    output.println("User was not registered.\n#END#");
                                    continue;
                                }
                            } else {
                                output.println("Username is not available.\n#END#");
                            }
                        } else {
                            output.println("User with username " + loggedInUsername + " is already logged in.\n#END#");
                        }
                    } else if (command.equalsIgnoreCase(LOG_IN_COMMAND)) {
                        char[] username = getUsername();
                        loggedInUsername = String.valueOf(username);
                        char[] password = getPassword();
                        AccountHelper accountHelper = new AccountHelper();
                        System.out.println(connectedUsers);
                        if (!isUserOnline(loggedInUsername)) {
                            if (accountHelper.isUserRegistered(String.valueOf(username), String.valueOf(password))) {
                                output.println("Welcome " + String.valueOf(username) + ", go ahead. Enter HELP to see all available commands.\n#END#");
                                connectedUsers.put(loggedInUsername, socket);
                                break;
                            } else {
                                output.println("There is no user with username " + String.valueOf(username) + ". Please try again or REGISTER yourself.\n#END#");
                                continue;
                            }
                        } else {
                            output.println("User with username " + loggedInUsername + " is already logged in.\n#END#");
                        }
                    } else {
                        output.println("You`re not allowed to use any other commands without authentication. Please REGISTER or LOG IN.\n#END#");
                    }
                    System.out.println("Serving " + client + " " + Thread.currentThread().getName());
                }
            }
            while (true) {
                if (input.ready()) {
                    String command = input.readLine();
                    System.out.println(command);
                    if (command.equalsIgnoreCase(BYE_COMMAND)) {
                        output.println("Bye, bye.\n#END#");
                        break;
                    }
                    if (command.equalsIgnoreCase(HELP_COMMAND)) {
                        output.println("Available commands :\n\"Follow the rules of the command`s syntax for a proper workflow. Use whitespaces where needed.\"");
                        int i = 1;
                        for (Map.Entry<String, String> e : HELP_COMMANDS.entrySet()) {
                            output.println(i + ". " + e.getKey() + "" + e.getValue());
                            i++;
                        }
                        output.println("\n#END#");
                    } else if (command.equalsIgnoreCase(CLR_HISTORY_COMMAND)) {
                        history = "";
                        output.println("History cleared. \n#END#");
                    } else if (command.startsWith(MSG_GROUP_COMMAND.toLowerCase())) {
                        if (ChatHelper.isEvaluableSendGroupMessageCommand(command)) {
                            String groupName = ChatHelper.getGroupNameFromSendGroupMessageCommand(command);
                            Map<String, Socket> group = new HashMap<>();
                            if (groups.keySet().contains(groupName)) {
                                for (Map.Entry<String, Map<String, Socket>> e : groups.entrySet()) {
                                    if (e.getKey().equals(groupName))
                                        group = e.getValue();
                                }
                                ChatHelper.sendGroupMessage(command, loggedInUsername, group);
                                output.println("Message sent. \n#END#");
                            } else {
                                output.println("There is no group named " + ChatHelper.getGroupNameFromSendGroupMessageCommand(command) + ".\n#END#");
                            }
                        } else {
                            output.println("Command MSG GROUP`s signature is wrong.\n#END#");
                        }
                    } else if (command.startsWith(MSG_COMMAND.toLowerCase())) {
                        if (ChatHelper.isEvaluableSendPrivateMessageCommand(command)) {
                            String[] tokens = command.split(" ");
                            if (isUserOnline(tokens[1])) {
                                ChatHelper.sendPrivateMessage(command, loggedInUsername);
                                output.println("Message sent. \n#END#");
                            } else {
                                output.println("User " + tokens[1] + " is offline.\n#END#");
                            }
                        } else if (ChatHelper.isEvaluableSendPublicMessageCommand(command)) {
                            ChatHelper.sendPublicMessage(command, loggedInUsername);
                            output.println("Message sent. \n#END#");
                        } else {
                            output.println("Command MSG`s signature is wrong.\n#END#");
                        }
                    } else if (command.startsWith(CREATE_GROUP_COMMAND.toLowerCase())) {
                        if (ChatHelper.isEvaluableCreateGroupCommand(command)) {
                            if (!groups.keySet().contains(ChatHelper.getGroupNameFromCreateCommand(command))) {
                                Map<String, Socket> group = new HashMap<String, Socket>();
                                groups.put(ChatHelper.getGroupNameFromCreateCommand(command), group);
                                System.out.println(groups);
                                isGroupAdministrativeRightsSet = true;
                                output.println("Group created. \n#END#");
                            } else {
                                output.println("There is already a group named " + ChatHelper.getGroupNameFromCreateCommand(command) + ".\n#END#");
                            }
                        } else {
                            output.println("Command CREATE GROUP`s signature is wrong.\n#END#");
                        }
                    } else if (command.startsWith(DELETE_GROUP_COMMAND.toLowerCase())) {
                        if (ChatHelper.isEvaluableDeleteGroupCommand(command)) {
                            if (isGroupAdministrativeRightsSet) {
                                if (groups.keySet().contains(ChatHelper.getGroupNameFromDeleteCommand(command))) {
                                    groups.remove(ChatHelper.getGroupNameFromDeleteCommand(command));
                                    System.out.println(groups);
                                    output.println("Group deleted. \n#END#");
                                } else {
                                    output.println("There is no group named " + ChatHelper.getGroupNameFromCreateCommand(command) + ".\n#END#");
                                }
                            } else {
                                output.println("You don`t have the rights to delete this group.\n#END#");
                            }
                        } else {
                            output.println("Command DELETE GROUP`s signature is wrong.\n#END#");
                        }
                    } else if (command.startsWith(ADD_USER_IN_GROUP_COMMAND.toLowerCase())) {
                        if (ChatHelper.isEvaluableAddUserInGroupCommand(command)) {
                            String groupName = ChatHelper.getGroupNameFromAddUserInGroupCommand(command);
                            if (groups.keySet().contains(groupName)) {
                                String username = ChatHelper.getUsernameFromAddUserInGroupCommand(command);
                                if (isUserOnline(username)) {
                                    Socket userSocket = null;
                                    for (Map.Entry<String, Socket> e : connectedUsers.entrySet()) {
                                        if (e.getKey().equals(username))
                                            userSocket = e.getValue();
                                    }
                                    for (Map.Entry<String, Map<String, Socket>> e : groups.entrySet()) {
                                        if (e.getKey().equals(groupName))
                                            (e.getValue()).put(username, userSocket);
                                    }
                                    output.println(username + " added to group " + groupName + ".\n#END#");
                                    System.out.println(groups);
                                } else {
                                    output.println("User " + username + " is offline.\n#END#");
                                }
                            } else {
                                output.println("There is no such group named " + groupName + ".\n#END#");
                            }
                        } else {
                            output.println("Command ADD USER IN GROUP`s signature is wrong.\n#END#");
                        }
                    } else if (command.startsWith(FIBONACCI_COMMAND.toLowerCase())) {
                        if (MathHelper.isEvaluableSpecial(command)) {
                            ArrayList<Integer> fibonacciSequence = MathHelper.getFibonacciSequence(command);
                            for (int i : fibonacciSequence)
                                output.print(i + " ");
                            output.println("\n#END#");
                        } else {
                            output.println("Command FIBONACCI`s signature is wrong.\n#END#");
                        }
                    } else if (command.startsWith(PRIME_COMMAND.toLowerCase())) {
                        if (MathHelper.isEvaluableSpecial(command)) {
                            ArrayList<Integer> primeSequence = MathHelper.getPrimeSequence(command);
                            for (int i : primeSequence)
                                output.print(i + " ");
                            output.println("\n#END#");
                        } else {
                            output.println("Command PRIME`s signature is wrong.\n#END#");
                        }
                    } else if (command.startsWith(PALINDROME_COMMAND.toLowerCase())) {
                        if (MathHelper.isEvaluableSpecial(command)) {
                            ArrayList<Integer> palindromeSequence = MathHelper.getPalindromeSequence(command);
                            for (int i : palindromeSequence)
                                output.print(i + " ");
                            output.println("\n#END#");
                        } else {
                            output.println("Command PALINDROME`s signature is wrong.\n#END#");
                        }
                    } else if (command.startsWith(CALC_COMMAND.toLowerCase())) {
                        if (MathHelper.isEvaluableBinary(command)) {
                            String result = MathHelper.evaluateBinaryExpression(command) + "";
                            updateHistory(command, result);
                            output.println(result + "\n#END#");
                        } else {
                            if (MathHelper.isEvaluableUnary(command)) {
                                String result = MathHelper.evaluateUnaryExpression(command) + "";
                                updateHistory(command, result);
                                output.println(result + "\n#END#");
                            } else
                                output.println("Command CALC`s signature is wrong.\n#END#");
                        }
                    } else if (command.equalsIgnoreCase(HISTORY_COMMAND)) {
                        if (history.isEmpty()) {
                            output.println("No entries. \n#END#");
                        } else {
                            output.println(history + "\n#END#");
                        }
                    } else if (command.equalsIgnoreCase(CONSTANTS_COMMAND)) {
                        output.println(MathHelper.getConstants() + "\n#END#");
                    } else {
                        output.println("Command not found, try HELP to see all available commands.\n#END#");
                    }
                    System.out.println("Serving " + client + " " + Thread.currentThread().getName());
                }
            }
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean isUserOnline(String username) {
        for (Map.Entry<String, Socket> e : connectedUsers.entrySet()) {
            if (e.getKey().equals(username))
                return true;
        }
        return false;
    }

    private char[] getPassword() throws IOException {
        output.println("PASSWORD\n#END#");
        char[] password = input.readLine().toCharArray();
        System.out.println(String.valueOf(password));
        return password;
    }

    private char[] getUsername() throws IOException {
        output.println("USERNAME\n#END#");
        char[] username = input.readLine().toCharArray();
        System.out.println(String.valueOf(username));
        return username;
    }

    private void updateHistory(String command, String result) {
        history += history.isEmpty() ? (command) : ("\n" + command);
        history += "\n" + result;
    }
}
