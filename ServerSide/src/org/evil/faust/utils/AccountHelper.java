package org.evil.faust.utils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.util.Arrays;
import java.util.Base64;

/**
 * Created by Faust on 4/9/2017.
 */
public class AccountHelper {
    private Cipher cipher;
    private SecretKey passwordSecretKey;
    private SecretKey usernameSecretKey;
    private KeyGenerator keyGenerator;
    private String encryptedPassword;
    private String encryptedUsername;

    public boolean registerUser(char[] username, char[] password) throws Exception {
        keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128);
        passwordSecretKey = keyGenerator.generateKey();
        usernameSecretKey = keyGenerator.generateKey();

        cipher = Cipher.getInstance("AES");

        encryptedPassword = encrypt(String.valueOf(password), passwordSecretKey);
        Arrays.fill(password, ' ');
        encryptedUsername = encrypt(String.valueOf(username), usernameSecretKey);
        Arrays.fill(username, ' ');

        return saveUser(encryptedUsername, encryptedPassword, usernameSecretKey, passwordSecretKey);
    }

    private String encrypt(String text, SecretKey secretKey)
            throws Exception {
        cipher = Cipher.getInstance("AES");
        byte[] plainTextByte = text.getBytes();
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedByte = cipher.doFinal(plainTextByte);
        Base64.Encoder encoder = Base64.getEncoder();
        String encryptedText = encoder.encodeToString(encryptedByte);
        return encryptedText;
    }

    private String decrypt(String encryptedText, SecretKey secretKey)
            throws Exception {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] encryptedTextByte = decoder.decode(encryptedText);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
        String decryptedText = new String(decryptedByte);
        return decryptedText;
    }

    private boolean saveUser(String username, String password, SecretKey usernameSecretKey, SecretKey passwordSecretKey) throws IOException {
        BufferedWriter out = new BufferedWriter(new FileWriter("config.txt", true));
        String base64EncodedUsernameKey = Base64.getEncoder().withoutPadding().encodeToString(usernameSecretKey.getEncoded());
        String base64EncodedPasswordKey = Base64.getEncoder().withoutPadding().encodeToString(passwordSecretKey.getEncoded());
        out.newLine();
        out.write(username + " # " + password + " # " + base64EncodedUsernameKey + " # " + base64EncodedPasswordKey);
        out.newLine();
        out.close();
        return true;
    }

    public boolean isUserRegistered(String username, String password) {
        String sCurrentLine = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("config.txt"));
            while ((sCurrentLine = br.readLine()) != null) {
                if (!sCurrentLine.isEmpty()) {
                    String[] tokens = sCurrentLine.split(" # ");
                    SecretKey usernameKey = new SecretKeySpec(Base64.getDecoder().decode(tokens[2]), "AES");
                    //System.out.println(usernameKey);
                    SecretKey passwordKey = new SecretKeySpec(Base64.getDecoder().decode(tokens[3]), "AES");
                    //System.out.println(passwordKey);
                    try {
                        if (tokens[0].equals(encrypt(username, usernameKey)) && tokens[1].equals(encrypt(password, passwordKey))) {
                            br.close();
                            return true;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    continue;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isUsernameAvailable(String username) {
        String sCurrentLine = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader("config.txt"));
            while ((sCurrentLine = br.readLine()) != null) {
                if (!sCurrentLine.isEmpty()) {
                    String[] tokens = sCurrentLine.split(" # ");
                    SecretKey usernameKey = new SecretKeySpec(Base64.getDecoder().decode(tokens[2]), "AES");
                    //System.out.println(usernameKey);
                    try {
                        if (tokens[0].equals(encrypt(username, usernameKey))) {
                            br.close();
                            return false;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    continue;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
