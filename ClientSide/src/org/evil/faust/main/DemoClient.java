package org.evil.faust.main;

/**
 * Created by Faust on 4/8/2017.
 */

import org.evil.faust.utils.MathClient;

public class DemoClient {

    public static void main(String[] args) {
        MathClient mathClient = new MathClient("localhost", 314);
        mathClient.openConnection();
        mathClient.startCommunication();
    }
}
