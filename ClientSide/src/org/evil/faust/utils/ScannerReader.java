package org.evil.faust.utils;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Faust on 4/11/2017.
 */
public class ScannerReader extends Thread {
    private ArrayList<Observer> observers = new ArrayList<>();
    private Scanner scanner = new Scanner(System.in);

    @Override
    public void run() {
        while (true)
            if (scanner.hasNext())
                publishCommand(scanner.nextLine());
    }

    private void publishCommand(String command) {
        for (Observer o : observers)
            o.consumeCommand(command);
    }

    public void registerObserver(Observer o) {
        observers.add(o);
    }
}
