package org.evil.faust.utils;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created by Faust on 4/8/2017.
 */
public class MathClient implements Observer{
    private Socket socket;
    private String server;
    private int port;
    private InetAddress ip;
    private BufferedReader input;
    private PrintWriter output;
    private String response = "";
    private String command = "";

    public MathClient(String server, int port) {
        this.server = server;
        this.port = port;
    }

    public void startCommunication() {
        try {
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

            System.out.println(input.readLine());

            ScannerReader scannerReader = new ScannerReader();
            scannerReader.registerObserver(this);
            scannerReader.setDaemon(true);
            scannerReader.start();

            while (true) {
                response = "";
                while (!socket.isClosed()) {
                    response = validateOneLine();
                    System.out.println(response);
                    if (response.equals("#END#"))
                        break;
                }

                if (command.equalsIgnoreCase("bye")) {
                    output.println("bye");
                    socket.close();
                    break;
                }
            }
        } catch (IOException e) {
            System.out.println("Lost connection with server.");
        }
    }

    public void openConnection() {
        System.out.println("Establishing connection... please wait.");
        while (socket == null) {
            try {
                ip = InetAddress.getByName(server);
                Socket socket = new Socket(ip, port);
                System.out.println("Server is online.");
                System.out.println("Connection established on " + server + ":" + port);
                this.socket = socket;
            } catch (UnknownHostException e) {
                System.out.println("The server is unknown.");
            } catch (IOException e) {
                System.out.println("Server is offline.");
            }
        }
    }

    private String validateOneLine() throws IOException {
        String response = input.readLine();
        if (response == null)
            return null;
        return response;
    }

    @Override
    public void consumeCommand(String command) {
        this.command = command;
        output.println(command);
    }
}


