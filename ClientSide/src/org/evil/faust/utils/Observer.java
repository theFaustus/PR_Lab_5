package org.evil.faust.utils;

/**
 * Created by Faust on 4/11/2017.
 */
public interface Observer {
    void consumeCommand(String command);
}
